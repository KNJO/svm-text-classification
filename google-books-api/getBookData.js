const axios = require("axios");
const csv = require("fast-csv");
const fs = require("fs");

const timeout = async ms => new Promise(resolve => setTimeout(resolve, ms));

const getBookDetails = async bookIsbn => {
  // Request with book ISBN
  const response = await axios.get(
    "https://www.googleapis.com/books/v1/volumes",
    {
      params: {
        q: `isbn${bookIsbn}`,
      },
    },
  );
  const book = await response.data;
  return book;
};

const createBooksJson = async () => {
  // Creating write stream for JSON output
  const writeStream = fs.createWriteStream("./books.json");
  writeStream.on("error", error => {
    console.log(error);
  });

  writeStream.write("[");

  const csvStream = csv
    .parseFile("../data/goodreads.csv", {
      headers: true,
      quote: null,
      escape: '"',
      discardUnmappedColumns: true,
    })
    .on("data", async row => {
      csvStream.pause();
      console.log(row.bookID, row.title);
      await timeout(500);
      try {
        const bookDetails = await getBookDetails(row.isbn);
        let jsonRow = JSON.stringify(bookDetails);
        jsonRow += ",";
        writeStream.write(jsonRow);
      } catch (error) {
        console.log(`Could not write ${row.id}: ${error}`);
      }
      csvStream.resume();
    })
    .on("error", error => console.error(error))
    .on("end", rowCount => {
      writeStream.write("]");
      writeStream.end();
      console.log(`Parsed ${rowCount} rows`);
    });
};

(async () => {
  await createBooksJson();

  console.log("Done!");
})();
